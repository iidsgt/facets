FROM vanallenlab/miniconda:3.6

RUN apt-get update
RUN apt-get -y install gcc gfortran git texlive 

RUN conda update --all
RUN conda update -n base conda
RUN conda config --add channels bioconda
RUN conda config --add channels conda-forge 

RUN conda install --yes r r-openssl r-devtools r-ggplot2
RUN conda install --yes snp-pileup=0.5.14

RUN git clone --branch 'v0.5.14' https://github.com/mskcc/facets.git
RUN ln -s /bin/tar /bin/gtar
RUN Rscript -e "options(unzip = 'internal'); devtools::install_github('mskcc/facets', build_vignettes = TRUE)"

COPY facets.R /
